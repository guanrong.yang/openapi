# Bees360 API (Alpha)

This documented API is not available for public yet. We will open this API to public in late January.

## Domain name

http://api.bees360.io

## Response Body and Status Code

Successful response example:



```js
200 OK
{
    project: {
        projectId: 10001,
    }
}
```

Error response example:

```js
400 BAD_REQUEST
{
    error: {
        message: "invailid project ID."
    }
}
```

## Authentication

### `POST /v1/oauth/token`

To authenticate with Bees360 API, first request a `client_id` and a `client_secret` from Bees360 development team.

With the `client_id` and `client_secret`, a client can send a http `POST` request to acquire `access_token` and `refresh_token`. For example,

```
curl -X POST -vu myClientId:myClientSecret http://api.bees360.io/v1/oauth/token \
     -H "Accept: application/json" \
     -d "password=mypassword&username=myusername&grant_type=password&scope=mycompanyname"
```

A successful authorization results in the following JSON response:

```
{
  "access_token": "ff16372e-38a7-4e29-88c2-1fb92897f558",
  "token_type": "bearer",
  "refresh_token": "f554d386-0b0a-461b-bdb2-292831cecd57",
  "expires_in": 43199,
  "scope": "read write"
}
```

## Project API

### Create new project

`POST /v1/projects`

Request body:

```js
{
    "country": "USA",  // required
    "state": "NJ",  // required
    "zipcode": "07302",  // required
    "streetAddress": "125 Hudson St, APT 708", // required
    "houseType": "residential_single_family",
    "dueDate": "01/31/2020",
    "policyNumber": "1234567",
    "inspectionNumber": "12345678",
    "insuredName": "Jane Doe",
    "insuredPhone": "2232131877",
    "agentName": "John Smith",
    "agentPhone": "1234567891",
    "reports": [ 
        {
            "reportName": "Roof-only Underwriting Report", //  require at least one report
        }
    ]
}
```

Example reponse:
```js
{
    projects: [
        {
            "id": 10001,
            "country": "USA",
            "state": "NJ",
            "zipcode": "07302",
            "streetAddress": "125 Hudson St, APT 708",
            "houseType": "residential_single_family",
            "dueDate": "01/31/2020",
            "policyNumber": "1234567",
            "inspectionNumber": "12345678",
            "insuredName": "Jane Doe",
            "insuredPhone": "2232131877",
            "agentName": "John Smith",
            "agentPhone": "1234567891",
            "reports": [ 
                {
                    "id": 0, // zero indicates the report has not been generated yet
                    "reportName": "Roof-only Underwriting Report",
                }
            ]
        }
    ]
}
```

Valid values for house type:

```js
[
    "residential_single_family",
    "residential_condo",
    "residential_townhouse",
    "residential_multi_family",
    "residential_apartments",
    "commercial",
]
```

### Get project latest status

`GET /v1/projects/{projectId}/latestStatus`

Example response:

```js
{
    project: [
        id: 10001,
        latestStatus: "returned_to_client", // this is the final status for a project.
    ]
}
```

Possible values for `latestStatus`:

```js
[
    "project_created",
    "customer_contacted",
    "assigned_to_pilot",
    "site_inspected",
    "returned_to_client",
]
```

### Get project images

`GET /v1/projects/{projectId}/images/archive`

Will return a redirect response, which redirects to a link to download the images archive in `.zip` format.

Example response:
```
302 FOUND
Content-Type: application/zip
Location: https://api.bees360.io/projects/12345/original_images_xxxxxx.zip
...
```

### Get project report file

`GET /v1/reports/{reportId}/file`

Will return a redirect response, which redirects to a link to download the report file in `.pdf` format.

```
302 FOUND
Content-Type: application/pdf
Location: https://api.bees360.io/reports/12345/roof_only_underwriting_report_xxxxxx.pdf
...
```
